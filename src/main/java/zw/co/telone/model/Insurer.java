package zw.co.telone.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import zw.co.telone.constants.StatusCode;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQuery(name = "Insurer.findByNamedQuery",
        query="select a from Insurer a where a.insurerName = :insurerName")
public class Insurer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(unique = true)
    private String insurerName;
    private String insurerLogo;
    private String address;
    private String secondAddress;
    private String mobileNumber;
    private String officeNumber;
    private String email;
    private String websiteUrl;
    private Boolean isActive;
    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime modifiedAt;
    @OneToMany(mappedBy = "insurer")
    @JsonManagedReference
    private List<Product> products;
    @OneToMany(mappedBy = "insurer")
    private List<VehicleInsurance> insurances;
}
