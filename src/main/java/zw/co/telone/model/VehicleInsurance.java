package zw.co.telone.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "vehicle_insurance")
public class VehicleInsurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "insurer_id")
    private Insurer insurer;
    private String policyType;
    private Integer insuranceTerm;
    private BigDecimal insurancePrice;
    private Boolean isActive;
    @CreationTimestamp
    private LocalDateTime createdAt;
}
