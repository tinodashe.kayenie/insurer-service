package zw.co.telone.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VehicleInfo {
    private String vehicle_regNumber;
    private String vehicle_make;
    private String vehicle_model;
    private LocalDate vehicle_year;
    private String valuedAt;
}
