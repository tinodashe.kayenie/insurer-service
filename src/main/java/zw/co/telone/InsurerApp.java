package zw.co.telone;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Main class

 * {@code @Authors} ={Tinodashe Kayenie}
 *
 */
@SpringBootApplication
@OpenAPIDefinition(info = @Info(
        title = "Insurer Microservice",
        description = "This Microservice manages Insurers and Their products",
        version = "0.0.1"))
@Slf4j
public class InsurerApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(InsurerApp.class, args);

        log.info("Chirikumhanya Ichii!");
    }
}
