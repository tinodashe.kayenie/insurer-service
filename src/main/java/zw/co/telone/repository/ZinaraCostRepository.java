package zw.co.telone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.telone.model.ZinaraCost;

import java.util.List;

@Repository
public interface ZinaraCostRepository extends JpaRepository<ZinaraCost, Long> {
    List<ZinaraCost> findByVehicleClassAndTerm(String vehicleClass, Integer term);
}
