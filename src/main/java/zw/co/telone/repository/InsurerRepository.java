package zw.co.telone.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.telone.model.Insurer;
import zw.co.telone.model.ProductCategory;

import java.util.List;

@Repository
public interface InsurerRepository extends JpaRepository<Insurer, Long> {
    List<Insurer> findByProductsCategory(ProductCategory productCategory);

    List<Insurer> findByIsActive(boolean isActive);

    Insurer findByInsurerName(String insurerName);

    @Transactional
    Insurer findByNamedQuery(@Param("insurerName") String insurerName);
}
