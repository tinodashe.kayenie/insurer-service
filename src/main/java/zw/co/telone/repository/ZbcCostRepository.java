package zw.co.telone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.telone.model.ZbcCost;

import java.util.List;

@Repository
public interface ZbcCostRepository extends JpaRepository<ZbcCost, Long> {
    List<ZbcCost> findByCategoryAndTerm(String category, Integer term);

}
