package zw.co.telone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.telone.model.VehicleInsurance;

import java.util.List;

@Repository
public interface VehicleInsuranceRepository extends JpaRepository<VehicleInsurance, Long> {

    List<VehicleInsurance> findByInsuranceTerm(Integer insuranceTerm);

    VehicleInsurance findVehicleInsuranceByInsuranceTerm(Integer insuranceTerm);
}
