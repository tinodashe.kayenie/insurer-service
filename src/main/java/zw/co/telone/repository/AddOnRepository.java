package zw.co.telone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.telone.model.AddOn;

@Repository
public interface AddOnRepository extends JpaRepository<AddOn, Long> {
}
