package zw.co.telone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import zw.co.telone.constants.StatusCode;
import zw.co.telone.model.Insurer;
import zw.co.telone.model.Product;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findProductsByInsurer(Insurer insurer);

    List<Product> findByIsActive(boolean isActive);

    Optional<Product> findProductByIdAndIsActive(Long productId, boolean isActive);
}
