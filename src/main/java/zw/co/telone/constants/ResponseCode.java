package zw.co.telone.constants;
@SuppressWarnings("unused")
public enum ResponseCode {
    SUCCESS("200"),

    CREATED("201"),

    NOT_FOUND("404"),
    FAILURE("400");
    @SuppressWarnings("unused")
    ResponseCode(String s) {

    }
}
