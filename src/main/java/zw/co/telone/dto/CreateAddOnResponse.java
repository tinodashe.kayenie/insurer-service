package zw.co.telone.dto;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateAddOnResponse {
    private Long addOnId;
    private String description;
    private Long productId;
    private Boolean isActive;
    @CreationTimestamp
    private LocalDateTime createdAt;

}
