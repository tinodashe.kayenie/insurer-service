package zw.co.telone.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateVehicleInsuranceRequest {
    private String policeType;
    private Long insurerId;
    private Integer insuranceTerm;
    private BigDecimal insurancePrice;
    private Boolean isActive;
    private LocalDateTime createdAt;
}
