package zw.co.telone.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "product creation Response")
public class CreateProductResponse {
    @SchemaProperty
    private Long productId;
    @SchemaProperty
    private String  productName;
    private BigDecimal price;
    @SchemaProperty
    private Long categoryId;
    @SchemaProperty
    private Long insurerId;
    private Boolean isActive;
    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

}
