package zw.co.telone.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "create insurer response")
public class CreateInsurerResponse {
    @SchemaProperty
    private Long insurerId;
    @SchemaProperty
    private String insurerName;
    @SchemaProperty
    private String insurerLogo;
    @SchemaProperty
    private String address;
    @SchemaProperty
    private String secondAddress;
    @SchemaProperty
    private String mobileNumber;
    @SchemaProperty
    private String officeNumber;
    @SchemaProperty
    private String email;
    @SchemaProperty
    private String websiteUrl;
    private Boolean isActive;
    @CreationTimestamp
    private LocalDateTime createdAt;

}
