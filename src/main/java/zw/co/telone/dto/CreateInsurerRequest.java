package zw.co.telone.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "create insurer payload")
public class CreateInsurerRequest {
    @SchemaProperty
    private String insurerName;
    @SchemaProperty
    private String insurerLogo;
    @SchemaProperty
    private String address;
    @SchemaProperty
    private String secondAddress;
    @SchemaProperty
    private String mobileNumber;
    @SchemaProperty
    private String officeNumber;
    @SchemaProperty
    private String email;
    @SchemaProperty
    private String websiteUrl;
    @SchemaProperty
    private Boolean isActive;
}
