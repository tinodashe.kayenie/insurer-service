package zw.co.telone.dto;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductCategoryResponse {
    private Long categoryId;
    private String description;
    private Boolean isActive;
    @CreationTimestamp
    private LocalDateTime createdAt;

}
