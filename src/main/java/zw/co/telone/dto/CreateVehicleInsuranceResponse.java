package zw.co.telone.dto;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateVehicleInsuranceResponse {

    private Long vehicleInsuranceId;
    private String insurerName;
    private String name;
    private Integer insuranceTerm;
    private BigDecimal insurancePrice;
    private Boolean isActive;
    @CreationTimestamp
    private LocalDateTime createdAt;


}
