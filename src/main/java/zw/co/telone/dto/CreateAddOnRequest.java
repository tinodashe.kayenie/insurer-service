package zw.co.telone.dto;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateAddOnRequest {

    private String description;
    private Boolean isActive;
    private Long productId;

}
