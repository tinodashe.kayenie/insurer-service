package zw.co.telone.dto;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class CreateQuotationResponse {

    private String insurerName;
    private Long insurerId;
    private String imageLogo;
    private String insuranceType;
    private Integer insuranceTerm;
    private Integer zbcTerm;
    private Integer zinaraTerm;
    private BigDecimal insurancePrice;
    private BigDecimal zbcPrice;
    private BigDecimal zinaraPrice;
    private BigDecimal totalPrice;
    @CreationTimestamp
    private LocalDateTime createdAt;


}
