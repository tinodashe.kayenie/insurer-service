package zw.co.telone.dto;

import jakarta.persistence.Column;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateQuotationRequest {
    private String category;
    private String vehicleClass;
    private Integer zbcTerm;
    private Integer zinaraTerm;
    private String insuranceType;
    private Integer insuranceTerm;
}
