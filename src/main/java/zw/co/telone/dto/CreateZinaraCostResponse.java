package zw.co.telone.dto;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateZinaraCostResponse {
    private Long id;
    private String vehicleClass;
    private BigDecimal usd_price;
    private BigDecimal zwl_price;
    private Integer term;
    private Boolean isActive;
    @CreationTimestamp
    private LocalDateTime createdAt;

}
