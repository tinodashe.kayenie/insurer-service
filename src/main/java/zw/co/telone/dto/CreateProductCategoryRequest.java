package zw.co.telone.dto;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductCategoryRequest {

    private String description;
    private Boolean isActive;

}
