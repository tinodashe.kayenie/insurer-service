package zw.co.telone.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Create Product Payload")
public class CreateProductRequest {
    @SchemaProperty
    private String productName;
    private BigDecimal price;
    @SchemaProperty
    private Long productCategoryId;
    @SchemaProperty
    private Long insurerId;
    private Boolean isActive;
    private LocalDateTime updatedAt;


}
