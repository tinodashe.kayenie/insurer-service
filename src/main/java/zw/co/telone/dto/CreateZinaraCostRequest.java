package zw.co.telone.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateZinaraCostRequest {
    private String vehicleClass;
    private BigDecimal usd_price;
    private BigDecimal zwl_price;
    private Integer term;
    private Boolean isActive;

}
