package zw.co.telone.service;

import zw.co.telone.dto.CreateZinaraCostRequest;
import zw.co.telone.dto.CreateZinaraCostResponse;

import java.util.List;

public interface ZinaraCostService {
    CreateZinaraCostResponse createZinaraCost(CreateZinaraCostRequest request);
    List<CreateZinaraCostResponse> getAllZinaraCosts();
}
