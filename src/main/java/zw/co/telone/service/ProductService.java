package zw.co.telone.service;

import zw.co.telone.constants.StatusCode;
import zw.co.telone.dto.CreateProductRequest;
import zw.co.telone.dto.CreateProductResponse;
import zw.co.telone.exceptions.ProductException;
import zw.co.telone.model.Insurer;
import zw.co.telone.model.Product;
import zw.co.telone.model.ProductCategory;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    CreateProductResponse createProduct(CreateProductRequest request);

    List<Product> getAllProductsWithAddOns();

    List<CreateProductResponse> getAllProducts();

    CreateProductResponse updateProduct(Long productId, CreateProductRequest request);

    void deleteProduct(Long productId) throws ProductException;

    List<CreateProductResponse> findProductsByInsurer(Insurer insurer);

    List<CreateProductResponse> getProductsByStatusCode(boolean isActive);

    Optional<CreateProductResponse> getProductById(Long productId);

    Optional<CreateProductResponse> findProductByIdAndStatus(Long productId, boolean isActive);
}
