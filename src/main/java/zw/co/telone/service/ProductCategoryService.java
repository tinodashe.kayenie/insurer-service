package zw.co.telone.service;

import zw.co.telone.constants.StatusCode;
import zw.co.telone.dto.CreateProductCategoryRequest;
import zw.co.telone.dto.CreateProductCategoryResponse;
import zw.co.telone.model.ProductCategory;

import java.util.List;

public interface ProductCategoryService {
CreateProductCategoryResponse createProductCategory(CreateProductCategoryRequest request);
List<ProductCategory> getAllProductCategories();

CreateProductCategoryResponse updateCategory(Long categoryId, CreateProductCategoryRequest request);

void deleteCategory(Long categoryId);
List<ProductCategory> getCategoriesByStatusCode(boolean isActive);
}
