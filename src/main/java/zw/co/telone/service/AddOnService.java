package zw.co.telone.service;

import zw.co.telone.dto.CreateAddOnRequest;
import zw.co.telone.dto.CreateAddOnResponse;
import zw.co.telone.model.AddOn;

import java.util.List;

public interface AddOnService {

    CreateAddOnResponse createAddOn(CreateAddOnRequest request);
//    List<AddOn> getAllAddOns();
    List<CreateAddOnResponse> getAllAddOns();

    CreateAddOnResponse updateAddOn(Long addOnId, CreateAddOnRequest request);

    void deleteAddOn(Long addOnId);
}
