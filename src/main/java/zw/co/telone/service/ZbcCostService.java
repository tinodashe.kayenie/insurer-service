package zw.co.telone.service;

import zw.co.telone.dto.CreateZbcCostRequest;
import zw.co.telone.dto.CreateZbcCostResponse;

import java.util.List;

public interface ZbcCostService {
    CreateZbcCostResponse createZbcCost(CreateZbcCostRequest request);
   List<CreateZbcCostResponse> getAllZbcCosts();
}
