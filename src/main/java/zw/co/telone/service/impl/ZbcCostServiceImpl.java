package zw.co.telone.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import zw.co.telone.dto.CreateZbcCostRequest;
import zw.co.telone.dto.CreateZbcCostResponse;
import zw.co.telone.model.ZbcCost;
import zw.co.telone.repository.ZbcCostRepository;
import zw.co.telone.service.ZbcCostService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ZbcCostServiceImpl implements ZbcCostService {

    private final ZbcCostRepository zbcCostRepository;
    @Override
    public CreateZbcCostResponse createZbcCost(CreateZbcCostRequest request) {

        var zbcCosts = ZbcCost.builder()
                .category(request.getZbcCategory())
                .term(request.getTerm())
                .usd_price(request.getUsd_price())
                .zwl_price(request.getZwl_price())
                .isActive(request.getIsActive())
                .build();
        var savedZbcCosts = zbcCostRepository.save(zbcCosts);
        return CreateZbcCostResponse.builder()
                .id(savedZbcCosts.getId())
                .category(savedZbcCosts.getCategory())
                .term(savedZbcCosts.getTerm())
                .usd_price(savedZbcCosts.getUsd_price())
                .zwl_price(savedZbcCosts.getZwl_price())
                .isActive(savedZbcCosts.getIsActive())
                .createdAt(savedZbcCosts.getCreatedAt())
                .build();
    }

    @Override
    public List<CreateZbcCostResponse> getAllZbcCosts() {
        List<ZbcCost> zbcCosts = zbcCostRepository.findAll();
        return zbcCosts.stream().map(this::mapToCreateZbcCostResponse).toList();
    }

    private  CreateZbcCostResponse mapToCreateZbcCostResponse(ZbcCost zbcCost){
        return CreateZbcCostResponse.builder()
                .id(zbcCost.getId())
                .category(zbcCost.getCategory())
                .term(zbcCost.getTerm())
                .usd_price(zbcCost.getUsd_price())
                .zwl_price(zbcCost.getZwl_price())
                .isActive(zbcCost.getIsActive())
                .build();
    }
}
