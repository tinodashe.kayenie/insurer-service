package zw.co.telone.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import zw.co.telone.dto.CreateProductRequest;
import zw.co.telone.dto.CreateProductResponse;
import zw.co.telone.model.Insurer;
import zw.co.telone.model.Product;
import zw.co.telone.model.ProductCategory;
import zw.co.telone.repository.ProductRepository;
import zw.co.telone.service.ProductService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

    private  final ProductRepository productRepository;
    @Override
    public CreateProductResponse createProduct(CreateProductRequest request) {


        var insurer = new Insurer();
        insurer.setId(request.getInsurerId());

        var category = new ProductCategory();
        category.setId(request.getProductCategoryId());

        Product product = Product.builder()
                .productName(request.getProductName())
                .price(request.getPrice())
                .insurer(insurer)
                .category(category)
                .isActive(request.getIsActive())
                .build();
        var createdProduct = productRepository.save(product);

        return CreateProductResponse.builder()
                .productId(createdProduct.getId())
                .productName(createdProduct.getProductName())
                .insurerId(createdProduct.getInsurer().getId())
                .categoryId(createdProduct.getCategory().getId())
                .isActive(createdProduct.getIsActive())
                .createdAt(createdProduct.getCreatedAt())
                .build();
    }

    @Override
    public List<Product> getAllProductsWithAddOns() {
        return productRepository.findAll();
    }

    @Override
    public List<CreateProductResponse> getAllProducts() {
        List<Product> products = productRepository.findAll();
        return products.stream().map(this::mapToCreateProductResponse).toList();
    }


    @Override
    public CreateProductResponse updateProduct(Long productId, CreateProductRequest request) {

        var insurer = new Insurer();
        insurer.setId(request.getInsurerId());

        var category = new ProductCategory();
        category.setId(request.getProductCategoryId());

        Optional<Product> existingProduct = productRepository.findById(productId);
        if(existingProduct.isPresent()) {
            Product product = existingProduct.get();
            product.setProductName(request.getProductName());
            product.setPrice(request.getPrice());
            product.setCategory(category);
            product.setInsurer(insurer);
            product.setIsActive(request.getIsActive());


            Product updatedProduct = productRepository.save(product);
            return CreateProductResponse.builder()
                    .productId(updatedProduct.getId())
                    .productName(updatedProduct.getProductName())
                    .price(updatedProduct.getPrice())
                    .insurerId(updatedProduct.getInsurer().getId())
                    .categoryId(updatedProduct.getCategory().getId())
                    .isActive(updatedProduct.getIsActive())
                    .updatedAt(updatedProduct.getUpdatedAt())
                    .build();
        }
        return null;
    }

    @Override
    public void deleteProduct(Long productId) {
        Optional<Product> productIdExists = productRepository.findById(productId);
        if(productIdExists.isPresent()) {
            productRepository.deleteById(productId);
            log.info("The entity is deleted successfully!");
        }
        log.info("The entity you want to delete does not exist!");
    }


    @Override
    public List<CreateProductResponse> findProductsByInsurer(Insurer insurer) {
        List<Product> products = productRepository.findProductsByInsurer(insurer);
        return products.stream().map(this::mapToCreateProductResponse).toList();
    }


    @Override
    public List<CreateProductResponse> getProductsByStatusCode(boolean isActive) {
        List<Product> products =  productRepository.findByIsActive(isActive);
        return products.stream().map(this::mapToCreateProductResponse).toList();
    }

    @Override
    public Optional<CreateProductResponse> getProductById(Long productId) {
        Optional<Product> product = productRepository.findById(productId);
        return product.map(this::mapToCreateProductResponse);
    }

    @Override
    public Optional<CreateProductResponse> findProductByIdAndStatus(Long productId, boolean isActive) {
       Optional<Product> product = productRepository.findProductByIdAndIsActive(productId, isActive);
        return product.map(this::mapToCreateProductResponse);
    }

    private CreateProductResponse mapToCreateProductResponse(Product product){
        return CreateProductResponse.builder()
                .productId(product.getId())
                .productName(product.getProductName())
                .price(product.getPrice())
                .insurerId(product.getInsurer().getId())
                .categoryId(product.getCategory().getId())
                .isActive(product.getIsActive())
                .build();
    }

}