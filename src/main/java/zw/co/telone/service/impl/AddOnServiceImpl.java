package zw.co.telone.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import zw.co.telone.dto.CreateAddOnRequest;
import zw.co.telone.dto.CreateAddOnResponse;
import zw.co.telone.model.AddOn;
import zw.co.telone.model.Product;
import zw.co.telone.repository.AddOnRepository;
import zw.co.telone.service.AddOnService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AddOnServiceImpl implements AddOnService {

    private final AddOnRepository addOnRepository;

    @Override
    public CreateAddOnResponse createAddOn(CreateAddOnRequest request) {

        var product = new Product();
        product.setId(request.getProductId());

        var createAddOn = AddOn.builder()
                .description(request.getDescription())
                .isActive(request.getIsActive())
                .product(product)
                .build();
        addOnRepository.save(createAddOn);
        return CreateAddOnResponse.builder()
                .addOnId(createAddOn.getId())
                .description(createAddOn.getDescription())
                .isActive(createAddOn.getIsActive())
                .productId(createAddOn.getProduct().getId())
                .build();
    }


    @Override
    public List<CreateAddOnResponse> getAllAddOns() {

        List<AddOn> addOns = addOnRepository.findAll();
        return addOns.stream().map(this::mapToCreateAddOnResponse).toList();

    }

    @Override
    public CreateAddOnResponse updateAddOn(Long addOnId, CreateAddOnRequest request) {

        var product = new Product();
        product.setId(request.getProductId());

        Optional<AddOn> existingAddOn = addOnRepository.findById(addOnId);
        if(existingAddOn.isPresent()){
            AddOn addOn = existingAddOn.get();
            addOn.setDescription(request.getDescription());
            addOn.setProduct(product);
            addOn.setIsActive(request.getIsActive());

           var updatedAddOn = addOnRepository.save(addOn);

            return CreateAddOnResponse.builder()
                    .addOnId(updatedAddOn.getId())
                    .description(updatedAddOn.getDescription())
                    .productId(updatedAddOn.getProduct().getId())
                    .isActive(updatedAddOn.getIsActive())
                    .build();
        }

        return null;

    }
    @Override
    public void deleteAddOn(Long addOnId) {
        if(addOnId!=null) {
            addOnRepository.deleteById(addOnId);
            log.info("The entity is deleted successfully!");
        }
        log.info("The entity you want to delete does not exist!");
    }

    private CreateAddOnResponse mapToCreateAddOnResponse(AddOn addOn){
        return CreateAddOnResponse.builder()
                .addOnId(addOn.getId())
                .description(addOn.getDescription())
                .productId(addOn.getProduct().getId())
                .isActive(addOn.getIsActive())
                .createdAt(addOn.getCreatedAt())
                .build();
    }
}