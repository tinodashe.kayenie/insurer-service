package zw.co.telone.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import zw.co.telone.dto.CreateZinaraCostRequest;
import zw.co.telone.dto.CreateZinaraCostResponse;
import zw.co.telone.model.ZinaraCost;
import zw.co.telone.repository.ZinaraCostRepository;
import zw.co.telone.service.ZinaraCostService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ZinaraCostServiceImpl implements ZinaraCostService {

    private final ZinaraCostRepository zinaraCostRepository;

    @Override
    public CreateZinaraCostResponse createZinaraCost(CreateZinaraCostRequest request) {
        var zinaraCosts = ZinaraCost.builder()
                .vehicleClass(request.getVehicleClass())
                .term(request.getTerm())
                .usd_price(request.getUsd_price())
                .zwl_price(request.getZwl_price())
                .isActive(request.getIsActive())
                .build();
        var savedZinaraCosts = zinaraCostRepository.save(zinaraCosts);
        return CreateZinaraCostResponse.builder()
                .id(savedZinaraCosts.getId())
                .vehicleClass(savedZinaraCosts.getVehicleClass())
                .term(savedZinaraCosts.getTerm())
                .usd_price(savedZinaraCosts.getUsd_price())
                .zwl_price(savedZinaraCosts.getZwl_price())
                .isActive(zinaraCosts.getIsActive())
                .createdAt(savedZinaraCosts.getCreatedAt())
                .build();
    }


    @Override
    public List<CreateZinaraCostResponse> getAllZinaraCosts() {
        List<ZinaraCost> zinaraCosts = zinaraCostRepository.findAll();
        return zinaraCosts.stream().map(this::mapToCreateZinaraCostResponse).toList();
    }

    private CreateZinaraCostResponse mapToCreateZinaraCostResponse(ZinaraCost zinaraCost) {
        return CreateZinaraCostResponse.builder()
                .id(zinaraCost.getId())
                .vehicleClass(zinaraCost.getVehicleClass())
                .term(zinaraCost.getTerm())
                .usd_price(zinaraCost.getUsd_price())
                .zwl_price(zinaraCost.getZwl_price())
                .isActive(zinaraCost.getIsActive())
                .build();
    }
}
