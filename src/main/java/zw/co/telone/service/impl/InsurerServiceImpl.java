package zw.co.telone.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import zw.co.telone.constants.StatusCode;
import zw.co.telone.dto.CreateInsurerRequest;
import zw.co.telone.dto.CreateInsurerResponse;
import zw.co.telone.model.Insurer;
import zw.co.telone.model.ProductCategory;
import zw.co.telone.repository.InsurerRepository;
import zw.co.telone.service.InsurerService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InsurerServiceImpl implements InsurerService {
    private final InsurerRepository insurerRepository;


    @Override
    public CreateInsurerResponse createInsurer(CreateInsurerRequest request) {
        Insurer insurer = Insurer.builder()
                .insurerName(request.getInsurerName())
                .insurerLogo(request.getInsurerLogo())
                .address(request.getAddress())
                .secondAddress(request.getSecondAddress())
                .mobileNumber(request.getMobileNumber())
                .officeNumber(request.getOfficeNumber())
                .email(request.getEmail())
                .websiteUrl(request.getWebsiteUrl())
                .isActive(request.getIsActive())
                .build();


        Insurer createdInsurer = insurerRepository.save(insurer);

        return CreateInsurerResponse.builder()
                .insurerId(insurer.getId())
                .insurerName(createdInsurer.getInsurerName())
                .insurerLogo(createdInsurer.getInsurerLogo())
                .address(createdInsurer.getAddress())
                .secondAddress(createdInsurer.getSecondAddress())
                .mobileNumber(createdInsurer.getMobileNumber())
                .officeNumber(createdInsurer.getOfficeNumber())
                .email(createdInsurer.getEmail())
                .websiteUrl(createdInsurer.getWebsiteUrl())
                .isActive(createdInsurer.getIsActive())
                .createdAt(createdInsurer.getCreatedAt())
                .build();
    }

    @Override
    public Optional<CreateInsurerResponse> getInsurerById(Long id) {
        Optional<Insurer> insurer = insurerRepository.findById(id);
        return insurer.map(this::mapToCreateInsurerResponse);
    }
    
    @Override
    public CreateInsurerResponse updateInsurer(Long id, CreateInsurerRequest request) {
        Optional<Insurer> existingInsurer = insurerRepository.findById(id);
        if (existingInsurer.isPresent()) {
            Insurer insurer = getInsurer(request, existingInsurer);
            Insurer updatedInsurer = insurerRepository.save(insurer);
            return CreateInsurerResponse.builder()
                    .insurerId(updatedInsurer.getId())
                    .insurerName(updatedInsurer.getInsurerName())
                    .insurerLogo(updatedInsurer.getInsurerLogo())
                    .address(updatedInsurer.getAddress())
                    .secondAddress(updatedInsurer.getSecondAddress())
                    .mobileNumber(updatedInsurer.getMobileNumber())
                    .officeNumber(updatedInsurer.getOfficeNumber())
                    .email(updatedInsurer.getEmail())
                    .websiteUrl(updatedInsurer.getWebsiteUrl())
                    .isActive(updatedInsurer.getIsActive())
                    .build();
        } else {
            return null;
        }
    }

    private static Insurer getInsurer(CreateInsurerRequest request, Optional<Insurer> existingInsurer) {
        Insurer insurer = existingInsurer.get();
        insurer.setInsurerName(request.getInsurerName());
        insurer.setInsurerLogo(request.getInsurerLogo());
        insurer.setAddress(request.getAddress());
        insurer.setSecondAddress(request.getSecondAddress());
        insurer.setMobileNumber(request.getMobileNumber());
        insurer.setOfficeNumber(request.getOfficeNumber());
        insurer.setEmail(request.getEmail());
        insurer.setWebsiteUrl(request.getWebsiteUrl());
        insurer.setIsActive(request.getIsActive());
        return insurer;
    }

    @Override
    public void deleteInsurer(Long id) {
        insurerRepository.deleteById(id);
    }

    @Override
    public List<Insurer> findInsurersByProductCategory(ProductCategory productCategory) {
        return insurerRepository.findByProductsCategory(productCategory);
    }

    @Override
    public List<CreateInsurerResponse> getInsurersByStatusCode(boolean isActive) {
        List<Insurer> insurers =  insurerRepository.findByIsActive(isActive);
        return insurers.stream().map(this::mapToCreateInsurerResponse).toList();
    }


    @Override
    public List<CreateInsurerResponse> findAllInsurers() {
        List<Insurer> insurers = insurerRepository.findAll();
        return insurers.stream().map(this::mapToCreateInsurerResponse)
                .toList();
    }


    private CreateInsurerResponse mapToCreateInsurerResponse(Insurer insurer) {
        return CreateInsurerResponse.builder()
                .insurerId(insurer.getId())
                .insurerName(insurer.getInsurerName())
                .insurerLogo(insurer.getInsurerLogo())
                .address(insurer.getAddress())
                .secondAddress(insurer.getSecondAddress())
                .mobileNumber(insurer.getMobileNumber())
                .officeNumber(insurer.getOfficeNumber())
                .email(insurer.getEmail())
                .websiteUrl(insurer.getWebsiteUrl())
                .isActive(insurer.getIsActive())
                .createdAt(insurer.getCreatedAt())
                .build();
    }

}
