package zw.co.telone.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import zw.co.telone.dto.CreateQuotationResponse;
import zw.co.telone.dto.CreateVehicleInsuranceRequest;
import zw.co.telone.dto.CreateVehicleInsuranceResponse;
import zw.co.telone.model.Insurer;
import zw.co.telone.model.VehicleInsurance;
import zw.co.telone.model.ZbcCost;
import zw.co.telone.model.ZinaraCost;
import zw.co.telone.repository.InsurerRepository;
import zw.co.telone.repository.VehicleInsuranceRepository;
import zw.co.telone.repository.ZbcCostRepository;
import zw.co.telone.repository.ZinaraCostRepository;
import zw.co.telone.service.VehicleInsuranceService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@SuppressWarnings("unsused")
public class VehicleInsuranceServiceImpl implements VehicleInsuranceService {

    private final VehicleInsuranceRepository vehicleInsuranceRepository;

    private final ZbcCostRepository zbcCostRepository;


    private final ZinaraCostRepository zinaraCostRepository;

    private final InsurerRepository insurerRepository;

    @Override
    public CreateVehicleInsuranceResponse createMotorVehicleProduct(CreateVehicleInsuranceRequest request) {

        Insurer insurer = new Insurer();
        insurer.setId(request.getInsurerId());


        VehicleInsurance vehicleInsurance = VehicleInsurance.builder()
                .policyType(request.getPoliceType())
                .insurer(insurer)
                .insuranceTerm(request.getInsuranceTerm())
                .insurancePrice(request.getInsurancePrice())
                .isActive(request.getIsActive())
                .createdAt(request.getCreatedAt())
                .build();


        var savedInsurance = vehicleInsuranceRepository.save(vehicleInsurance);

        Insurer insurerName = insurerRepository.findById(request.getInsurerId()).orElse(null);


        assert insurerName != null;
        return CreateVehicleInsuranceResponse.builder()
                .vehicleInsuranceId(savedInsurance.getId())
                .insurerName(insurerName.getInsurerName())
                .name(savedInsurance.getPolicyType())
                .insuranceTerm(savedInsurance.getInsuranceTerm())
                .insurancePrice(savedInsurance.getInsurancePrice())
                .isActive(savedInsurance.getIsActive())
                .createdAt(savedInsurance.getCreatedAt())
                .build();
    }

    @Override
    public List<CreateVehicleInsuranceResponse> getAllVehicleProduct() {
        List<VehicleInsurance> insurances = vehicleInsuranceRepository.findAll();
        return insurances.stream().map(this::mapToCreateVehicleInsuranceResponse)
                .toList();
    }

    @Override
    public List<CreateQuotationResponse> getQuotationBy(String category, String vehicleClass, Integer zbcTerm, Integer zinaraTerm, String insuranceType, Integer insuranceTerm) {

        // Fetch the ZbcCost and ZinaraCost objects from the database
        List<ZbcCost> zbcCostList = zbcCostRepository.findByCategoryAndTerm(category, zbcTerm);
        List<ZinaraCost> zinaraCostList = zinaraCostRepository.findByVehicleClassAndTerm(vehicleClass, zinaraTerm);
        List<VehicleInsurance> insuranceCostList = vehicleInsuranceRepository.findByInsuranceTerm(insuranceTerm);

        List<CreateQuotationResponse> responseList = new ArrayList<>();

        // Perform the calculations for each combination of costs
        for (ZbcCost zbcCost : zbcCostList) {
            for (ZinaraCost zinaraCost : zinaraCostList) {
                for (VehicleInsurance insuranceCost : insuranceCostList) {
                    BigDecimal total_price = BigDecimal.valueOf(0);

                    BigDecimal zbcTermMultiplier = BigDecimal.valueOf(zbcTerm.equals(1) ? 1 : zbcTerm);
                    BigDecimal zinaraTermMultiplier = BigDecimal.valueOf(zinaraTerm.equals(1) ? 1 : zinaraTerm);
                    BigDecimal insuranceTermMultiplier = BigDecimal.valueOf(insuranceTerm.equals(1) ? 1 : insuranceTerm);

                    total_price = total_price.add(zbcCost.getUsd_price())
                            .add(zinaraCost.getUsd_price())
                            .add(insuranceCost.getInsurancePrice());

                    // Retrieve the insurer linked to the insurance
                    Insurer insurer = insuranceCost.getInsurer();

                    // Create the quotation response for the current combination of costs
                    CreateQuotationResponse response = new CreateQuotationResponse();
                    response.setInsurerName(insurer.getInsurerName());
                    response.setInsurerId(insurer.getId());
                    response.setImageLogo(insurer.getInsurerLogo());
                    response.setZbcTerm(zbcCost.getTerm());
                    response.setZinaraTerm(zinaraCost.getTerm());
                    response.setInsuranceType(insuranceCost.getPolicyType());
                    response.setInsuranceTerm(insuranceCost.getInsuranceTerm());
                    response.setInsurancePrice(insuranceCost.getInsurancePrice());
                    response.setZbcPrice(zbcCost.getUsd_price());
                    response.setZinaraPrice(zinaraCost.getUsd_price());
                    response.setTotalPrice(total_price);
                    response.setCreatedAt(LocalDateTime.now().withMinute(1));
                    responseList.add(response);
                }
            }
        }

        return responseList;
    }

    private CreateVehicleInsuranceResponse mapToCreateVehicleInsuranceResponse(VehicleInsurance vehicleInsurance) {
        Insurer insurer = new Insurer();
        insurer.setId(vehicleInsurance.getInsurer().getId());
        Insurer insurerName = insurerRepository.findById(insurer.getId()).orElse(null);
        assert insurerName != null;
        return CreateVehicleInsuranceResponse.builder()
                .vehicleInsuranceId(vehicleInsurance.getId())
                .insurerName(insurerName.getInsurerName())
                .name(vehicleInsurance.getPolicyType())
                .insuranceTerm(vehicleInsurance.getInsuranceTerm())
                .insurancePrice(vehicleInsurance.getInsurancePrice())
                .isActive(vehicleInsurance.getIsActive())
                .createdAt(vehicleInsurance.getCreatedAt())
                .build();
    }
}
