package zw.co.telone.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import zw.co.telone.dto.CreateProductCategoryRequest;
import zw.co.telone.dto.CreateProductCategoryResponse;
import zw.co.telone.model.ProductCategory;
import zw.co.telone.repository.ProductCategoryRepository;
import zw.co.telone.service.ProductCategoryService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductCategoryServiceImpl implements ProductCategoryService {

    private final ProductCategoryRepository productCategoryRepository;
    @Override
    public CreateProductCategoryResponse createProductCategory(CreateProductCategoryRequest request) {
        var category = ProductCategory.builder()
                .description(request.getDescription())
                .isActive(request.getIsActive())
                .build();
        productCategoryRepository.save(category);
        return CreateProductCategoryResponse.builder()
                .categoryId(category.getId())
                .description(category.getDescription())
                .isActive(category.getIsActive())
                .build();
    }

    @Override
    public List<ProductCategory> getAllProductCategories() {
        return productCategoryRepository.findAll();
    }

    @Override
    public CreateProductCategoryResponse updateCategory(Long categoryId, CreateProductCategoryRequest request) {
        Optional<ProductCategory> existingProductCategory = productCategoryRepository.findById(categoryId);
        if(existingProductCategory.isPresent()){
            ProductCategory category = existingProductCategory.get();
            category.setDescription(request.getDescription());
            category.setIsActive(request.getIsActive());

            ProductCategory updatedCategory = productCategoryRepository.save(category);

            return CreateProductCategoryResponse.builder()
                    .categoryId(updatedCategory.getId())
                    .description(updatedCategory.getDescription())
                    .isActive(updatedCategory.getIsActive())
                    .build();
        }

        return null;
    }

    @Override
    public void deleteCategory(Long categoryId) {
        if(categoryId!=null) {
            productCategoryRepository.deleteById(categoryId);
            log.info("Category is deleted successfully!");
        }
        log.info("Category you want to delete does not exist!");
    }

    @Override
    public List<ProductCategory> getCategoriesByStatusCode(boolean isActive) {
        return productCategoryRepository.findByIsActive(isActive);
    }

}

