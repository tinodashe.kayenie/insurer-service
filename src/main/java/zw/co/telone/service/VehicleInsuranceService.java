package zw.co.telone.service;

import zw.co.telone.dto.CreateQuotationRequest;
import zw.co.telone.dto.CreateQuotationResponse;
import zw.co.telone.dto.CreateVehicleInsuranceRequest;
import zw.co.telone.dto.CreateVehicleInsuranceResponse;

import java.util.List;

public interface VehicleInsuranceService {
    CreateVehicleInsuranceResponse createMotorVehicleProduct(CreateVehicleInsuranceRequest request);
    List<CreateVehicleInsuranceResponse> getAllVehicleProduct();

    List<CreateQuotationResponse> getQuotationBy(String category, String vehicleClass, Integer zbcTerm,  Integer  zinaraTerm,  String  insuranceType, Integer  insuranceTerm);


}
