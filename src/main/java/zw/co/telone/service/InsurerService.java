package zw.co.telone.service;

import zw.co.telone.constants.StatusCode;
import zw.co.telone.dto.CreateInsurerRequest;
import zw.co.telone.dto.CreateInsurerResponse;
import zw.co.telone.dto.CreateProductResponse;
import zw.co.telone.model.Insurer;
import zw.co.telone.model.Product;
import zw.co.telone.model.ProductCategory;

import java.util.List;
import java.util.Optional;

public interface InsurerService {
    CreateInsurerResponse createInsurer(CreateInsurerRequest request);
    Optional<CreateInsurerResponse> getInsurerById(Long id);
    CreateInsurerResponse updateInsurer(Long id, CreateInsurerRequest request);
    void deleteInsurer(Long id);
    List<Insurer> findInsurersByProductCategory(ProductCategory productCategory);

//    List<Insurer> getInsurersByStatusCode(StatusCode statusCode);
    List<CreateInsurerResponse> getInsurersByStatusCode(boolean isActive);

    List<CreateInsurerResponse> findAllInsurers();
}
