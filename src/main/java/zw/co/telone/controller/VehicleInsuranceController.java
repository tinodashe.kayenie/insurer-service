package zw.co.telone.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import zw.co.telone.dto.CreateQuotationResponse;
import zw.co.telone.dto.CreateVehicleInsuranceRequest;
import zw.co.telone.dto.CreateVehicleInsuranceResponse;
import zw.co.telone.response.ApiResponse;
import zw.co.telone.service.VehicleInsuranceService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/vehicle-insurance")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class VehicleInsuranceController {

    private final VehicleInsuranceService vehicleInsuranceService;

    @PostMapping
    public ApiResponse<CreateVehicleInsuranceResponse> createMotorVehicleProduct(@RequestBody CreateVehicleInsuranceRequest request) {
        try {

            var insurance = vehicleInsuranceService.createMotorVehicleProduct(request);
            return new ApiResponse<>(HttpStatus.CREATED, insurance);

        } catch (Exception e) {
            return new ApiResponse<>(HttpStatus.BAD_REQUEST, null);
        }

    }

    @GetMapping
    public ApiResponse<List<CreateVehicleInsuranceResponse>> getAllVehicleInsurance() {
        List<CreateVehicleInsuranceResponse> insurance = vehicleInsuranceService.getAllVehicleProduct();
        return new ApiResponse<>(HttpStatus.OK, insurance);
    }

    @GetMapping("/quotes")
    public ApiResponse<List<CreateQuotationResponse>> getQuotationBy(

            @RequestParam(required = false) String zbcCategory,
            @RequestParam(required = false) String vehicleClass,
            @RequestParam(required = false) Integer zbcTerm,
            @RequestParam(required = false) Integer zinaraTerm,
            @RequestParam(required = false) String insuranceType,
            @RequestParam(required = false) Integer insuranceTerm) {

        try {
        List<CreateQuotationResponse> quotationResponse = vehicleInsuranceService.getQuotationBy(zbcCategory, vehicleClass, zbcTerm, zinaraTerm, insuranceType, insuranceTerm);

            return new ApiResponse<>(HttpStatus.OK, quotationResponse);
        } catch (HttpClientErrorException.BadRequest e) {
            return new ApiResponse<>(HttpStatus.BAD_REQUEST, null);
        }
    }
}
