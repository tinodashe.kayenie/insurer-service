package zw.co.telone.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import zw.co.telone.dto.CreateProductCategoryRequest;
import zw.co.telone.dto.CreateProductCategoryResponse;
import zw.co.telone.model.ProductCategory;
import zw.co.telone.repository.ProductCategoryRepository;
import zw.co.telone.response.ApiResponse;
import zw.co.telone.service.ProductCategoryService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@SuppressWarnings("unused")
public class ProductCategoryController {
    private final ProductCategoryService productCategoryService;
    private final ProductCategoryRepository productCategoryRepository;


    @PostMapping
    public ApiResponse<CreateProductCategoryResponse> createProductCategory(
            @RequestBody CreateProductCategoryRequest request){
        try{
        var category = productCategoryService.createProductCategory(request);
        return  new ApiResponse<>(HttpStatus.CREATED, category);
    } catch (Exception e) {
        return new ApiResponse<>(HttpStatus.INTERNAL_SERVER_ERROR, null);
    }
    }

    @GetMapping
    public ApiResponse<List<ProductCategory>> getAllCategories(){
        var categories = productCategoryService.getAllProductCategories();
        if (categories.isEmpty()){
            return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
        }else{
        return new ApiResponse<>(HttpStatus.OK, categories);}
    }

    @PutMapping("/{categoryId}")
    public ApiResponse<CreateProductCategoryResponse> updateCategory(
            @PathVariable("categoryId") Long categoryId, @RequestBody CreateProductCategoryRequest request){
        boolean existsById = productCategoryRepository.existsById(categoryId);
        if (existsById){
            var updatedCategory = productCategoryService.updateCategory(categoryId, request);
            return new ApiResponse<>(HttpStatus.OK, updatedCategory);
        }
        return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
    }

    @DeleteMapping("/{categoryId}")
    public ApiResponse<Void> deleteCategory(@PathVariable("categoryId") Long categoryId){
        boolean exist = productCategoryRepository.existsById(categoryId);

        if(exist) {

            productCategoryService.deleteCategory(categoryId);
            return new ApiResponse<>(HttpStatus.NO_CONTENT, null);
        }

        return new ApiResponse<>(HttpStatus.NOT_FOUND,null);
    }

    @GetMapping("/status/{is-active}")
    public List<ProductCategory> getCategoriesByStatusCode(@PathVariable("is-active") boolean isActive) {
        return productCategoryService.getCategoriesByStatusCode(isActive);
    }
}

