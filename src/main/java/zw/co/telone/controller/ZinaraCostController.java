package zw.co.telone.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import zw.co.telone.dto.CreateZinaraCostRequest;
import zw.co.telone.dto.CreateZinaraCostResponse;
import zw.co.telone.response.ApiResponse;
import zw.co.telone.service.ZinaraCostService;

import java.util.List;

@RestController
@RequestMapping("api/v1/zinara")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class ZinaraCostController {
    private final ZinaraCostService zinaraCostService;

    @PostMapping
    public ApiResponse<CreateZinaraCostResponse> createZinaraCost(@RequestBody CreateZinaraCostRequest request){
        var zinaraCost = zinaraCostService.createZinaraCost(request);
        return new ApiResponse<>(HttpStatus.CREATED, zinaraCost);
    }

    @GetMapping
    public ApiResponse<List<CreateZinaraCostResponse>> getAllProducts() {
        var zinaraCosts = zinaraCostService.getAllZinaraCosts();
        if(!zinaraCosts.isEmpty()) {
            return new ApiResponse<>(HttpStatus.OK, zinaraCosts);
        }
        return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
    }
}

//Vehicle weight 4251 -5000kg - USD$90.
//Vehicle weight 5001-5750kg - USD$100.