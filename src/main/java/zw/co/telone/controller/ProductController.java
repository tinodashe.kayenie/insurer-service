package zw.co.telone.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import zw.co.telone.dto.CreateProductRequest;
import zw.co.telone.dto.CreateProductResponse;
import zw.co.telone.exceptions.ProductException;
import zw.co.telone.model.Insurer;
import zw.co.telone.model.Product;
import zw.co.telone.repository.ProductRepository;
import zw.co.telone.response.ApiResponse;
import zw.co.telone.service.ProductService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/products")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class ProductController {

    private final ProductService productService;
    private final ProductRepository productRepository;

    @PostMapping
    public ApiResponse<CreateProductResponse> createProduct(@RequestBody CreateProductRequest request) {
        var createdProduct = productService.createProduct(request);
        return new ApiResponse<>(HttpStatus.CREATED, createdProduct);
    }

    @GetMapping
    public ApiResponse<List<CreateProductResponse>> getAllProducts() {
        var products = productService.getAllProducts();
        if(!products.isEmpty()) {
            return new ApiResponse<>(HttpStatus.OK, products);
        }
        return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
    }

    @PutMapping("/{productId}")
    public ApiResponse<CreateProductResponse> updateProduct(
            @PathVariable("productId") Long productId,
            @RequestBody CreateProductRequest request){

        boolean existsById = productRepository.existsById(productId);

            if (existsById){
                var updatedProduct = productService.updateProduct(productId, request);
                return new ApiResponse<>(HttpStatus.OK, updatedProduct);
        }

            return new ApiResponse<>(HttpStatus.NOT_FOUND, null);

    }
    @DeleteMapping("/{productId}")
    public ApiResponse<Void> deleteProduct(@PathVariable("productId") Long productId) throws ProductException {
        boolean exist = productRepository.existsById(productId);

        if(exist) {

            productService.deleteProduct(productId);
            return new ApiResponse<>(HttpStatus.NO_CONTENT, null);
        }

           return new ApiResponse<>(HttpStatus.NOT_FOUND,null);
        }


    @GetMapping("/by-insurer/{insurerId}")
    public ApiResponse<List<CreateProductResponse>> getProductsByInsurer(@PathVariable("insurerId") Long insurerId) {

        Insurer productInsurer = new Insurer();
        productInsurer.setId(insurerId);

        List<CreateProductResponse> products = productService.findProductsByInsurer(productInsurer);

        if (products.isEmpty()) {
            return new ApiResponse<>(HttpStatus.NOT_FOUND, null); //ResponseEntity.noContent().build();
        }

        return new ApiResponse<>(HttpStatus.OK, products); //ResponseEntity.ok(insurers);

    }

    @GetMapping("/status")
    public List<CreateProductResponse> getProductsByStatusCode(@RequestParam boolean isActive) {
        return productService.getProductsByStatusCode(isActive);
    }

    @GetMapping("{productId}")
    public ApiResponse<Optional<CreateProductResponse>> getProductById(@PathVariable("productId") Long productId) {
        Optional<CreateProductResponse> product = productService.getProductById(productId);
        if (product.isPresent()){
        return new ApiResponse<>(HttpStatus.OK, product);
        }else {
            return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
        }

    }

    @GetMapping("/{productId}/{is-active}")
    public ApiResponse<Optional<CreateProductResponse>> findProductByIdAndStatus(
            @PathVariable("productId") Long productId, @PathVariable("is-active") boolean isActive ){
        Optional<CreateProductResponse> product = productService.findProductByIdAndStatus(productId, isActive);
        if (product.isPresent()) {
            return new ApiResponse<>(HttpStatus.OK, product);
        }else {
            return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
        }
    }

    @GetMapping("with-addons")
    public ApiResponse<List<Product>> findAllProductsWithAddOns(){
        var products =  productService.getAllProductsWithAddOns();

        if(!products.isEmpty()){
            return new ApiResponse<>(HttpStatus.OK, products);

        }else {
            return new ApiResponse<>(HttpStatus.NOT_FOUND, null);}
    }

}