package zw.co.telone.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import zw.co.telone.dto.CreateZbcCostRequest;
import zw.co.telone.dto.CreateZbcCostResponse;
import zw.co.telone.response.ApiResponse;
import zw.co.telone.service.ZbcCostService;

import java.util.List;
@RestController
@RequestMapping("/api/v1/zbc")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class ZbcCostController {

    private final ZbcCostService zbcCostService;

    @PostMapping
    public ApiResponse<CreateZbcCostResponse> createZinaraCost(@RequestBody CreateZbcCostRequest request){
        var zbcCost = zbcCostService.createZbcCost(request);
        return new ApiResponse<>(HttpStatus.CREATED, zbcCost);
    }

    @GetMapping
    public ApiResponse<List<CreateZbcCostResponse>> getAllProducts() {
        var zbcCosts = zbcCostService.getAllZbcCosts();
        if(!zbcCosts.isEmpty()) {
            return new ApiResponse<>(HttpStatus.OK, zbcCosts);
        }
        return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
    }
}
