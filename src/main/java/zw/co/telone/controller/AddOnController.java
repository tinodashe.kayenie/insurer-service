package zw.co.telone.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import zw.co.telone.constants.ResponseCode;
import zw.co.telone.dto.CreateAddOnRequest;
import zw.co.telone.dto.CreateAddOnResponse;
import zw.co.telone.model.AddOn;
import zw.co.telone.repository.AddOnRepository;
import zw.co.telone.response.ApiResponse;
import zw.co.telone.service.AddOnService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/add_ons")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@SuppressWarnings("unused")
public class AddOnController {
    private final AddOnRepository addOnRepository;
    private final AddOnService addOnService;

    @PostMapping
    public ApiResponse<CreateAddOnResponse> createAddOn(@RequestBody CreateAddOnRequest request) {
        var createAddOn = addOnService.createAddOn(request);
        return new ApiResponse<>(HttpStatus.CREATED, createAddOn);
    }

    @GetMapping
    public ApiResponse<List<CreateAddOnResponse>> getAllAddOns() {
        var addOns = addOnService.getAllAddOns();
        return new ApiResponse<>(HttpStatus.OK, addOns);
    }

    @PutMapping("/{addOnId}")
    public ApiResponse<CreateAddOnResponse> updateAddOn(@RequestParam("addOnId") Long addOnId, @RequestBody CreateAddOnRequest request) {
        var updatedAddOn = addOnService.updateAddOn(addOnId, request);

        if (updatedAddOn != null) {
            return new ApiResponse<>(HttpStatus.OK, updatedAddOn);
        } else {
            return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
        }
    }

    @DeleteMapping
    public ApiResponse<Void> deleteAddOn(@RequestParam("addOnId") Long addOnId) {
        boolean exist = addOnRepository.existsById(addOnId);
        if (exist){

            addOnService.deleteAddOn(addOnId);

        return new ApiResponse<>(HttpStatus.NO_CONTENT, null);
    }
            
        return new ApiResponse<>(HttpStatus.NOT_FOUND,null);
}
}
