package zw.co.telone.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import zw.co.telone.dto.CreateInsurerRequest;
import zw.co.telone.dto.CreateInsurerResponse;
import zw.co.telone.model.Insurer;
import zw.co.telone.model.ProductCategory;
import zw.co.telone.response.ApiResponse;
import zw.co.telone.service.InsurerService;

import java.nio.channels.CancelledKeyException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/insurers")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@SuppressWarnings("unused")

public class InsurerController {
    private final InsurerService insurerService;

    @PostMapping
    public ApiResponse<CreateInsurerResponse> createInsurer(@RequestBody CreateInsurerRequest request) {
        try {
            CreateInsurerResponse createdInsurer = insurerService.createInsurer(request);
            return new  ApiResponse<>(HttpStatus.CREATED, createdInsurer);

        }catch (CancelledKeyException e){
            return new ApiResponse<>(HttpStatus.BAD_REQUEST,null);
        }


    }

    @GetMapping("/{id}")
    public ResponseEntity<CreateInsurerResponse> getInsurerById(@PathVariable("id") Long id) {
        Optional<CreateInsurerResponse> insurer = insurerService.getInsurerById(id);
        return insurer.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    public ApiResponse<List<CreateInsurerResponse>> getAllInsurers() {
        List<CreateInsurerResponse> insurers = insurerService.findAllInsurers();
        return new ApiResponse<>(HttpStatus.OK, insurers);
    }

    @PutMapping("/{id}")
    public ApiResponse<CreateInsurerResponse> updateInsurer(
            @PathVariable("id") Long id,
            @RequestBody CreateInsurerRequest request) {
        CreateInsurerResponse updatedInsurer = insurerService.updateInsurer(id, request);
        if (updatedInsurer != null) {
            return new ApiResponse<>(HttpStatus.OK, updatedInsurer);
        } else {
            return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
        }
    }



    @DeleteMapping("/{id}")
    public ApiResponse<Void> deleteInsurer(@PathVariable("id") Long id) {
        insurerService.deleteInsurer(id);
        return new ApiResponse<>(HttpStatus.NO_CONTENT, null);
    }

    @GetMapping("/by-category/{categoryId}")
    public ApiResponse<List<Insurer>> getInsurersByCategory(@PathVariable Long categoryId) {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setId(categoryId);

        List<Insurer> insurers = insurerService.findInsurersByProductCategory(productCategory);

        if (insurers.isEmpty()) {
            return new ApiResponse<>(HttpStatus.NOT_FOUND, null);
        }

        return new ApiResponse<>(HttpStatus.OK,  insurers);
    }
    @GetMapping("/status/{is-active}")
    public List<CreateInsurerResponse> getInsurersByStatusCode(@PathVariable("is-active") boolean isActive) {
        return insurerService.getInsurersByStatusCode(isActive);
    }
}